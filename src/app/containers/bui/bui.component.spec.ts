import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuiComponent } from './bui.component';

describe('BuiComponent', () => {
  let component: BuiComponent;
  let fixture: ComponentFixture<BuiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
