import { Component, OnInit, OnDestroy } from '@angular/core';
import { BuiService } from '../../services/bui.service';
import { Central } from '../../interfaces/central.interface';
import { Section } from '../../interfaces/section.interface';
import { Room } from '../../interfaces/room.interface';
import { Device } from '../../interfaces/device.interface';
import { sortBy } from 'lodash';
import { Subscription } from 'rxjs';
import { DeviceType } from '../../enums/device-type.enum';

@Component({
  selector: 'app-bui',
  templateUrl: './bui.component.html',
  styleUrls: ['./bui.component.scss']
})
export class BuiComponent implements OnInit, OnDestroy {

  public centralInfo: Central;
  public sections: Section[];
  public allRooms: Room[];
  public devices: Device[];
  private centralSub: Subscription;
  private sectionsSub: Subscription;
  private roomsSub: Subscription;
  private devicesSub: Subscription;

  constructor(private buiService: BuiService) { }

  ngOnInit() {
    this.centralSub = this.buiService.getCentral()
      .subscribe((centralInfo: Central) => this.centralInfo = centralInfo);
    this.sectionsSub = this.buiService.getSections()
      .subscribe((sections: Section[]) => this.sections = sortBy([...sections], [(section: Section) => section.sortOrder]));
    this.roomsSub = this.buiService.getRooms()
      .subscribe((rooms: Room[]) => this.allRooms = sortBy([...rooms], [(room: Room) => room.sortOrder]));
      this.devicesSub = this.buiService.getDevices()
      .subscribe((devices: Device[]) => {
        const filtredDevices = devices
          .filter(device => {
            return device.type === DeviceType.BinaryLight || device.type === DeviceType.DimmableLight;
          });
        this.devices = sortBy([...filtredDevices], [(device: Device) => [device.sortOrder]]);
      });
  }

  ngOnDestroy() {
    this.centralSub.unsubscribe();
    this.sectionsSub.unsubscribe();
    this.roomsSub.unsubscribe();
    this.devicesSub.unsubscribe();
  }
}
