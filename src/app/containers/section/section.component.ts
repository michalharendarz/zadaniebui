import { Subscription } from 'rxjs';
import { BuiService } from './../../services/bui.service';
import { RefreshStateService } from './../../services/refresh-state.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Room } from '../../interfaces/room.interface';
import { Section } from '../../interfaces/section.interface';
import { Device } from '../../interfaces/device.interface';
import { DeviceProperties } from '../../enums/device-properties.enum';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit, OnDestroy {

  @Input() allRooms: Room[];
  @Input() sections: Section[];
  @Input() devices: Device[];

  public isDevicesVisible = false;
  public isRoomsVisible = false;
  public roomsToShow: Room[];
  public devicesToShow: Device[];
  private actionSub: Subscription;
  private refreshStateSub: Subscription;

  constructor(
      private refreshStateService: RefreshStateService,
      private buiService: BuiService
    ) { }

  ngOnInit(): void {
    this.refreshStateSub = this.refreshStateService.getLatestState().subscribe(state => {
      if (state && state.changes) {
        state.changes.forEach(element => {
          const device = this.devices.find(t => t.id === element.id);
          if (!device) {
            return;
          }
          device.properties.value = element.value;
        });
      }
    });
  }

  public getRoom(id: number): void {
    this.isDevicesVisible = false;
    this.isRoomsVisible = true;
    this.roomsToShow = this.allRooms.filter((room: Room) => room.sectionID === id);
  }

  public showDevices(id: number): void {
    this.isDevicesVisible = true;
    this.devicesToShow = this.devices.filter((device: Device) => device.roomID === id);
  }

  public changeDeviceValueBinary(device: Device): void {
    const action = device.properties.value === 1 ? DeviceProperties.Off : DeviceProperties.On;
    this.actionSub = this.buiService.callAction(device.id, action).subscribe(t => {
      this.refreshStateService.refresh();
    });

  }

  public changeDeviceValueDimmable(device: Device): void {
    const val = Math.round(Math.random() * 100);
    this.actionSub = this.buiService.callAction(device.id, 'setValue', val).subscribe(t => {
      this.refreshStateService.refresh();
    });
  }

  public ngOnDestroy(): void {
      this.actionSub.unsubscribe();
      this.refreshStateSub.unsubscribe();
  }
}
