export enum DeviceProperties {
  Off = 'turnOff',
  On = 'turnOn',
}
