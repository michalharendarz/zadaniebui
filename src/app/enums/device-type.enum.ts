export enum DeviceType {
  BinaryLight = 'binary_light',
  DimmableLight = 'dimmable_light',
}
