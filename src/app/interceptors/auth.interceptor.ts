import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url !== 'api/settings-info') {
      request = request.clone({
        setHeaders: {
          // TODO powinno zostać odczytane z jakiegoś providera
          Authorization: 'Basic ' + btoa('admin:admin')
        }
      });
      return next.handle(request);
    } else {
      return next.handle(request);
    }
  }
}
