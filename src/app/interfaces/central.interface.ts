export interface Central {
  serialNumber: string;
  mac: string;
  softVersion: string;
}
