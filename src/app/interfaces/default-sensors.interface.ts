export interface DefaultSensors {
  temperature: number;
  humidity: number;
  light: number;
}
