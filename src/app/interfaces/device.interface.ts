export interface Device {
  id: number;
  name: string;
  roomID: number;
  type: string;
  baseType: string;
  enabled: boolean;
  visible: boolean;
  isPlugin: boolean;
  parentId: number;
  remoteGatewayId: number;
  viewXml: boolean;
  configXml: boolean;
  interfaces: string[];
  properties: any;
  actions: any;
  created: string;
  modified: string;
  sortOrder: number;
}
