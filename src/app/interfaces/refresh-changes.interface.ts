export interface RefreshChanges {
  id: number;
  log: string[];
  logTemp: string;
  value: string;
  valueSensor: string;
}
