import { RefreshChanges } from './refresh-changes.interface';

export interface RefreshState {
  status: string;
  last: number;
  date: string;
  timestamp: number;
  logs: any[];
  changes: RefreshChanges[];
}
