import { DefaultSensors } from './default-sensors.interface';

export interface Room {
  id: number;
  sectionID: number;
  name: string;
  defaultSensors: DefaultSensors;
  defaultThermostat: number;
  sortOrder: number;
}
