export interface Section {
  id: number;
  name: string;
  sortOrder: number;
}
