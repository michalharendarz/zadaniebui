import { TestBed, inject } from '@angular/core/testing';

import { BuiService } from './bui.service';

describe('BuiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BuiService]
    });
  });

  it('should be created', inject([BuiService], (service: BuiService) => {
    expect(service).toBeTruthy();
  }));
});
