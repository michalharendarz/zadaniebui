import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Central } from '../interfaces/central.interface';
import { Section } from '../interfaces/section.interface';
import { Room } from '../interfaces/room.interface';
import { Device } from '../interfaces/device.interface';
import { RefreshState } from '../interfaces/refresh-state.interface';

@Injectable()
export class BuiService {

  constructor(private http: HttpClient) { }

  getCentral(): Observable<Central> {
    return this.http.get('api/settings-info').pipe(
      map((central: any) => central as Central)
    );
  }

  getSections(): Observable<Section[]> {
    return this.http.get('api/sections').pipe(
      map((section: any) => section as Section[])
    );
  }

  getRooms(): Observable<Room[]> {
    return this.http.get('api/rooms').pipe(
      map((room: any) => room as Room[])
    );
  }

  getDevices(): Observable<Device[]> {
    return this.http.get('api/devices').pipe(
      map((device: any) => device as Device[])
    );
  }

  callAction(deviceId: number, name: string, arg1?: any): Observable<boolean> {
    const arg1Part = arg1 ? `&arg1=${arg1}` : '';
    const url = `api/callAction?deviceID=${deviceId}&name=${name}` + arg1Part;
    return this.http.get<boolean>(url);
  }

  getRefreshState(id?: number): Observable<RefreshState> {
    const last = id;
    return this.http.get(`api/refreshStates?last=${last}`).pipe(
      map((device: any) => device as RefreshState)
    );
  }
}
