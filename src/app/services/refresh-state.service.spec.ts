import { TestBed } from '@angular/core/testing';

import { RefreshStateService } from './refresh-state.service';

describe('RefreshStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RefreshStateService = TestBed.get(RefreshStateService);
    expect(service).toBeTruthy();
  });
});
