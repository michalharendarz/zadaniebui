import { Observable, timer, merge, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { RefreshState } from '../interfaces/refresh-state.interface';
import { BuiService } from './bui.service';
import { mergeMap, mapTo, tap, map } from '../../../node_modules/rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RefreshStateService {

  private forceRefresh: Subject<number> = new Subject<number>();
  private currentRefreshId = 0;
  private isInitialCall = true;
  private mainStream: any;

  constructor(private buiService: BuiService) { }

  getLatestState(): Observable<RefreshState> {
    if (this.mainStream && !this.isInitialCall) {
      return this.mainStream;
    }
    const timerStream = timer(0, 30 * 1000).pipe(mapTo('source: Timer'));
    const forcStream = this.forceRefresh.asObservable().pipe(mapTo('source: Force'));
    const apiCall = this.buiService.getRefreshState(this.currentRefreshId).pipe(tap((response) => {
      this.currentRefreshId = response.last;
    }));
    const firstApiCall = apiCall.pipe(map((response) => {
      this.isInitialCall = false;
      return response.last;
    }), mergeMap((r) => {
      return apiCall;
    }));

    this.mainStream = merge(timerStream, forcStream).pipe(
      mergeMap(() => {
        if (!this.isInitialCall) {
          return this.buiService.getRefreshState(this.currentRefreshId).pipe(tap((response) => {
            this.currentRefreshId = response.last;
          }));
        }
        return firstApiCall;
      }));
    return this.mainStream;
  }

  public refresh(): void {
    this.forceRefresh.next(1);
  }
}
