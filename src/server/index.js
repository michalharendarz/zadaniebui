const express = require('express');
const app = express();

let last = 0;
let changes = [];

// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

let devices = [
  {
    "id": 100,
    "name": "Lampka",
    "roomID": 2,
    "type": "binary_light",
    "properties": { "dead": "0", "disabled": "0", "value": "1" },
    "actions": { "setValue": 1, "turnOff": 0, "turnOn": 0 },
    "sortOrder": 2
  },
  {
    "id": 105,
    "name": "TV",
    "roomID": 1,
    "type": "dimmable_light",
    "properties": { "dead": "0", "disabled": "0", "value": "100" },
    "actions": { "setValue": 1, "turnOff": 0, "turnOn": 0 }, "sortOrder": 1
  }
]

app.route('/settings-info').get((req, res) => {
  res.send(
    {
      "serialNumber": "HC2-021121",
      "hcName": "HC2-021121",
      "mac": "00:22:4d:b0:19:32",
      "softVersion": "4.180",
      "beta": false,
      "zwaveVersion": "3.67",
      "timeFormat": 24,
      "zwaveRegion": "EU",
      "serverStatus": 1539681223,
      "defaultLanguage": "en",
      "sunsetHour": "15:44",
      "sunriseHour": "07:43",
      "hotelMode": false,
      "updateStableAvailable": true,
      "temperatureUnit": "C",
      "newestStableVersion": "4.520",
      "updateBetaAvailable": false,
      "batteryLowNotification": true,
      "smsManagement": true,
      "date": "14:50 | 3.12.2018",
      "timestamp": 1543845047,
      "online": true,
      "recoveryCondition": "Ok"
    }
  );
});

app.route('/sections').get((req, res) => {
  res.send(
    [{
      "id": 4,
      "name": "Parter",
      "sortOrder": 1
    },
    {
      "id": 5,
      "name": "Piętro",
      "sortOrder": 2
    }
    ]);
});

app.route('/rooms').get((req, res) => {
  res.send(
    [{
      "id": 1,
      "name": "Kuchnia",
      "sectionID": 4,
      "icon": "",
      "defaultSensors": {
        "temperature": 24,
        "humidity": 0,
        "light": 0
      },
      "defaultThermostat": 0,
      "sortOrder": 1
    },
    {
      "id": 2,
      "name": "Łazienka",
      "sectionID": 4,
      "icon": "",
      "defaultSensors": {
        "temperature": 7,
        "humidity": 0,
        "light": 0
      },
      "defaultThermostat": 0,
      "sortOrder": 4
    },
    {
      "id": 6,
      "name": "Salon",
      "sectionID": 4,
      "icon": "",
      "defaultSensors": {
        "temperature": 25,
        "humidity": 0,
        "light": 28
      },
      "defaultThermostat": 0,
      "sortOrder": 2
    },
    {
      "id": 7,
      "name": "Sypialnia",
      "sectionID": 5,
      "icon": "",
      "defaultSensors": {
        "temperature": 23,
        "humidity": 0,
        "light": 0
      },
      "defaultThermostat": 0,
      "sortOrder": 3
    },
    {
      "id": 8,
      "name": "Łazienka",
      "sectionID": 5,
      "icon": "",
      "defaultSensors": {
        "temperature": 0,
        "humidity": 0,
        "light": 0
      },
      "defaultThermostat": 0,
      "sortOrder": 5
    }
    ]);
});

app.route('/devices').get((req, res) => {
  res.send(devices);
});

app.route('/callAction').get((req, res) => {
  const deviceId = req.query.deviceID;
  const action = req.query.name;
  let value = req.query.arg1;
  let device = devices.find(t => { return t.id == deviceId });
  if (!device) {
    console.log('cant find devices ', deviceId);
  }
  if (action !== 'setValue') {
    value = action === 'turnOn' ? 1 : 0;
  }
  device.properties.value = value;

  changes.push({
    "id": device.id,
    "log": "Komunikacja prawidowa",
    "logTemp": "TxtGreen",
    "value": value,
    "valueSensor": value
  });
  res.send(true);
});


app.route('/refreshStates').get((req, res) => {
  let lastParam = req.query.last || 0;
  last++;

  if (lastParam === 0) {
    res.send({
      'status': null,
      'last': last,
      'date': null,
      'timestamp': null,
      'logs': null,
      'changes': null
    });

  } else {
    var date = new Date();
    const response = {
      'status': 'IDLE',
      'last': last + 1,
      'date': getDate(),
      'timestamp': date.timestamp,
      'logs': [],
      'changes': [...changes]
    };
    console.log(response);
    res.send(response);
    changes = [];
  }
});

function getDate() {
  var date = new Date();
  return date.getHours() + ':' + date.getMinutes() + ' | ' + date.getDay() + '.' + date.getMonth() + '.' + date.getFullYear();
}
function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

app.listen(3000);
